---
# Parameterise AZ's
AWSTemplateFormatVersion: 2010-09-09

Parameters:
  AZOne:
    Type: AWS::EC2::AvailabilityZone::Name
    Default: "us-west-1b"
    Description: "first availability zone to use"
  AZTwo:
    Type: AWS::EC2::AvailabilityZone::Name
    Default: "us-west-1c"
    Description: "Second availability zone to use"
  AirportServiceVersion:
    Type: String
    Default: "1.0.1"

Resources:
  # Networking
  # ------------------------------ #
  VpcStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL: stacks/networking.yml

  # Services
  # ------------------------------ #
  CountryServiceStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL: stacks/service.yml
      Parameters:
        VpcId: !GetAtt [VpcStack, Outputs.VpcId]
        PrivateSubnetAZ1: !GetAtt [VpcStack, Outputs.PrivateSubnetAZ1]
        PrivateSubnetAZ2: !GetAtt [VpcStack, Outputs.PrivateSubnetAZ2]
        AZOne: !Ref AZOne
        AZTwo: !Ref AZTwo
        InstanceType: t2.micro
        PublicWebAccessSG: !Ref PublicWebAccessSG
        ALBListener: !Ref ALBListener
        TargetGroupListenerPriority: "3"
        ServiceJarName: countries-assembly-1.0.1.jar
        ServiceName: country-service
        PathPattern: "/countries*"

  AirportServiceStack:
    Type: "AWS::CloudFormation::Stack"
    Properties:
      TemplateURL: stacks/service.yml
      Parameters:
        VpcId: !GetAtt [VpcStack, Outputs.VpcId]
        PrivateSubnetAZ1: !GetAtt [VpcStack, Outputs.PrivateSubnetAZ1]
        PrivateSubnetAZ2: !GetAtt [VpcStack, Outputs.PrivateSubnetAZ2]
        AZOne: !Ref AZOne
        AZTwo: !Ref AZTwo
        InstanceType: t2.medium
        PublicWebAccessSG: !Ref PublicWebAccessSG
        ALBListener: !Ref ALBListener
        TargetGroupListenerPriority: "4"
        ServiceJarName: !Sub airports-assembly-${AirportServiceVersion}.jar
        ServiceName: airport-service
        PathPattern: "/airports*"

  # Application Load Balancer
  # ------------------------------ #
  ApplicationLoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Name: lt-assignment
      Subnets:
        - Fn::GetAtt: [VpcStack, Outputs.PublicSubnetAZ1]
        - Fn::GetAtt: [VpcStack, Outputs.PublicSubnetAZ2]
      SecurityGroups:
        - !Ref PublicWebAccessSG

  ALBListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      LoadBalancerArn: !Ref ApplicationLoadBalancer
      Port: 80
      Protocol: HTTP
      DefaultActions:
        - Type: forward
          TargetGroupArn: !Ref DefaultTargetGroup


  # We define a default target group here, as this is a mandatory Parameters
  # when creating an Application Load Balancer Listener. This is not used, instead
  # a target group is created per-service in each service template.
  DefaultTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      Name: default-target-group
      VpcId: !GetAtt [VpcStack, Outputs.VpcId]
      Port: 80
      Protocol: HTTP
      Tags:
        - Key: Name
          Value: "default-target-group"


  DefaultListenerRule:
    Type: AWS::ElasticLoadBalancingV2::ListenerRule
    Properties:
      ListenerArn: !Ref ALBListener
      Priority: 5
      Conditions:
        - Field: path-pattern
          Values:
            - "*"
      Actions:
        - TargetGroupArn: !GetAtt [AirportServiceStack, Outputs.ServiceTargetGroup]
          Type: forward


  PublicWebAccessSG:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: public-web-access-sg
      VpcId:
        Fn::GetAtt:
          - VpcStack
          - Outputs.VpcId
      GroupDescription: "Access to the service from the world"
      SecurityGroupIngress:
        - CidrIp: 0.0.0.0/0
          IpProtocol: tcp
          FromPort: 80
          ToPort: 80
      Tags:
        - Key: Name
          Value: public-web-access-sg

  PublicWebAccessEgressRulesCountryService:
    Type: "AWS::EC2::SecurityGroupEgress"
    Properties:
      GroupId: !Ref PublicWebAccessSG
      DestinationSecurityGroupId: !GetAtt [CountryServiceStack, Outputs.SecurityGroup]
      IpProtocol: tcp
      FromPort: 8080
      ToPort: 8080

  PublicWebAccessEgressRulesAirportService:
    Type: "AWS::EC2::SecurityGroupEgress"
    Properties:
      GroupId: !Ref PublicWebAccessSG
      DestinationSecurityGroupId: !GetAtt [AirportServiceStack, Outputs.SecurityGroup]
      IpProtocol: tcp
      FromPort: 8080
      ToPort: 8080
