FROM alpine

RUN apk --no-cache add \
        bash jq git curl python py-pip

RUN pip install --upgrade pip awscli cfn-lint

# Expose credentials volume
RUN mkdir ~/.aws
